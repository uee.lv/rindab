package lv.uee.rinda.dati;

import lv.uee.rinda.tips.RindaDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DbDatiTest {

    private DbDati dbDati = new DbDati();


    @Test
    public void sortTest() {
        List<RindaDto> rindaDtos = new ArrayList<>();

        RindaDto rindaDto1 = new RindaDto();
        rindaDto1.setRindaId(1L);
        rindaDto1.setRegistracijasDatumsRinda(new Date());
        rindaDto1.setPrioritate("");
        rindaDtos.add(rindaDto1);

        RindaDto rindaDto2 = new RindaDto();
        rindaDto2.setRindaId(2L);
        rindaDto2.setRegistracijasDatumsRinda(new Date());
        rindaDto2.setPrioritate("MĀSA");
        rindaDtos.add(rindaDto2);

        RindaDto rindaDto3 = new RindaDto();
        rindaDto3.setRindaId(3L);
        rindaDto3.setRegistracijasDatumsRinda(new Date());
        rindaDto3.setPrioritate("MĀSA");
        rindaDtos.add(rindaDto3);

        RindaDto rindaDto4 = new RindaDto();
        rindaDto4.setRindaId(4L);
        rindaDto4.setRegistracijasDatumsRinda(new Date());
        rindaDto4.setPrioritate("");
        rindaDtos.add(rindaDto4);

        List<RindaDto> rindaDtosSorted = dbDati.sortRinda(rindaDtos);
        assertEquals(rindaDtosSorted.get(0).getRindaId(), rindaDtos.get(1).getRindaId());
        assertEquals(rindaDtosSorted.get(1).getIestadeId(), rindaDtos.get(2).getIestadeId());
        assertEquals(rindaDtosSorted.get(2).getIestadeId(), rindaDtos.get(0).getIestadeId());
        assertEquals(rindaDtosSorted.get(3).getIestadeId(), rindaDtos.get(3).getIestadeId());
    }


}