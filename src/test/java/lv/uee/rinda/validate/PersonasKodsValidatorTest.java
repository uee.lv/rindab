package lv.uee.rinda.validate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonasKodsValidatorTest {

    private PersonasKodsValidator personasKodsValidator = new PersonasKodsValidator();

    @Test
    void personasKodsTest() {
        assertTrue(personasKodsValidator.isValid("000000-00000", null));
        assertFalse(personasKodsValidator.isValid("000000.00000", null));
        assertFalse(personasKodsValidator.isValid("000-00000000", null));
        assertFalse(personasKodsValidator.isValid("A00000-0000B", null));
        assertFalse(personasKodsValidator.isValid("00000000000", null));
        assertFalse(personasKodsValidator.isValid("000000000000", null));
        assertFalse(personasKodsValidator.isValid("0000000000000", null));
        assertFalse(personasKodsValidator.isValid("", null));
    }


}