package lv.uee.rinda.validate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrioritateValidatorTest {

    PrioritateValidator prioritateValidator = new PrioritateValidator();


    @Test
    void prioritateTest() {
        assertTrue(prioritateValidator.isValid("BRĀLIS", null));
        assertTrue(prioritateValidator.isValid("MĀSA", null));
        assertTrue(prioritateValidator.isValid("", null));
        assertFalse(prioritateValidator.isValid("brālis", null));
    }
}