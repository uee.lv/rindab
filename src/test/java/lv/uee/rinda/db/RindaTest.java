package lv.uee.rinda.db;

import lv.uee.rinda.db.model.Iestade;
import lv.uee.rinda.db.model.Rinda;
import lv.uee.rinda.db.repository.IestadeRepository;
import lv.uee.rinda.db.repository.RindaRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class RindaTest {

    @Autowired
    IestadeRepository iestadeRepository;

    @Autowired
    RindaRepository rindaRepository;

    @Test
    void rindaRepositoryIsLoaded() {
        assertNotNull(rindaRepository);
    }

    @Test
    void iestadeRepositoryIsLoaded() {
        assertNotNull(iestadeRepository);
    }

    @Test
    void saveSelectRinda() {
        Iestade iestade = this.iestadeRepository.save(Iestade.builder()
                .nosaukums("NOSAUKUMS")
                .adrese("ADRESSE")
                .build());
        Rinda rinda = this.rindaRepository.save(Rinda.builder()
                .iestadeId(iestade)
                .vards("VARDS")
                .uzvards("UZVARDS")
                .personasKods("111111-22222")
                .registracijasDatumsRinda(new Date())
                .prioritate("")
                .build());
        assertNotNull(rinda.getRindaId());
        assertNotNull(rindaRepository.findByIestadeIdIestadeIdAndPersonasKods(iestade.getIestadeId(), rinda.getPersonasKods()));
        assertNotNull(rindaRepository.findByIestadeIdIestadeId(iestade.getIestadeId()));
        rindaRepository.delete(rinda);
        assertEquals(Optional.empty(), rindaRepository.findById(rinda.getRindaId()));
    }

}