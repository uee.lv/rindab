package lv.uee.rinda.db;

import lv.uee.rinda.db.model.Iestade;
import lv.uee.rinda.db.repository.IestadeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
class IestadeTest {

    @Autowired
    IestadeRepository iestadeRepository;

    @Test
    void iestadeRepositoryIsLoaded() {
        assertNotNull(iestadeRepository);
    }

    @Test
    void saveIestade() {
        Iestade iestade = this.iestadeRepository.save(Iestade.builder()
                .nosaukums("NOSAUKUMS")
                .adrese("ADRESSE")
                .build());
        assertNotNull(iestade.getIestadeId());
        assertNotNull(iestadeRepository.findAll());
        iestadeRepository.delete(iestade);
        assertEquals(Optional.empty(), iestadeRepository.findById(iestade.getIestadeId()));
    }

}