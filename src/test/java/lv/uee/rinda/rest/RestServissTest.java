package lv.uee.rinda.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import lv.uee.rinda.db.model.Iestade;
import lv.uee.rinda.db.model.Rinda;
import lv.uee.rinda.db.repository.IestadeRepository;
import lv.uee.rinda.db.repository.RindaRepository;
import lv.uee.rinda.tips.PersonaDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class RestServissTest {

    @Autowired
    private WebApplicationContext applicationContext;

    @Autowired
    private IestadeRepository iestadeRepository;

    @Autowired
    private RindaRepository rindaRepository;

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(applicationContext)
                .build();
    }

    @Test
    void iestadesTest() throws Exception {
        this.iestadeRepository.save(Iestade.builder()
                .nosaukums("NOSAUKUMS")
                .adrese("ADRESSE")
                .build());
        this.mockMvc.perform(
                get("/iestade")
        ).andExpect(status().isOk());
    }

    @Test
    void iestadeTest() throws Exception {
        Iestade iestade = this.iestadeRepository.save(Iestade.builder()
                .nosaukums("NOSAUKUMS")
                .adrese("ADRESSE")
                .build());
        this.mockMvc.perform(
                get("/iestade/" + iestade.getIestadeId())
        ).andExpect(status().isOk());
    }

    @Test
    void iestadeNotFoundExceptionTest() throws Exception {
        this.mockMvc.perform(
                get("/iestade/0")
        ).andExpect(status().isNotFound());
    }

    @Test
    void iestadeInvalidParam() throws Exception {
        this.mockMvc.perform(
                get("/iestade/-")
        ).andExpect(status().isBadRequest());
    }

    @Test
    void rindaTest() throws Exception {
        Iestade iestade = this.iestadeRepository.save(Iestade.builder()
                .nosaukums("NOSAUKUMS")
                .adrese("ADRESSE")
                .build());
        Rinda rinda = this.rindaRepository.save(Rinda.builder()
                .iestadeId(iestade)
                .vards("VARDS")
                .uzvards("UZVARDS")
                .personasKods("111111-22222")
                .registracijasDatumsRinda(new Date())
                .prioritate("")
                .build());
        this.mockMvc.perform(
                get("/rinda/iestade/" + iestade.getIestadeId())
        ).andExpect(status().isOk());
    }

    @Test
    void rindaNotFoundExceptionTest() throws Exception {
        this.mockMvc.perform(
                get("/iestade/0")
        ).andExpect(status().isNotFound());
    }

    @Test
    void rindaInvalidParamTest() throws Exception {
        this.mockMvc.perform(
                get("/rinda/iestade/-")
        ).andExpect(status().isBadRequest());
    }

    @Test
    void rindaAddTest() throws Exception {
        Iestade iestade = this.iestadeRepository.save(Iestade.builder()
                .nosaukums("NOSAUKUMS")
                .adrese("ADRESSE")
                .build());
        PersonaDTO personaDTO = new PersonaDTO();
        personaDTO.setVards("PERSONA");
        personaDTO.setUzvards("UZVARDS");
        personaDTO.setPersonasKods("111111-00000");
        personaDTO.setPrioritate("");
        this.mockMvc.perform(
                post("/rinda/iestade/" + iestade.getIestadeId())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(personaDTO))
        ).andExpect(status().isOk());
        Rinda rinda = rindaRepository.findByIestadeIdIestadeIdAndPersonasKods(iestade.getIestadeId(), personaDTO.getPersonasKods()).get();
        assertEquals(rinda.getPersonasKods(), personaDTO.getPersonasKods());
    }


    @Test
    void rindaAddPersonasKodsTest() throws Exception {
        Iestade iestade = this.iestadeRepository.save(Iestade.builder()
                .nosaukums("NOSAUKUMS")
                .adrese("ADRESSE")
                .build());
        PersonaDTO personaDTO = new PersonaDTO();
        personaDTO.setVards("PERSONA");
        personaDTO.setUzvards("UZVARDS");
        personaDTO.setPersonasKods("11111-100000");
        personaDTO.setPrioritate("");
        this.mockMvc.perform(
                post("/rinda/iestade/" + iestade.getIestadeId())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(personaDTO))
        ).andExpect(status().isBadRequest());
    }

    @Test
    void rindaDeleteTest() throws Exception {
        Iestade iestade = this.iestadeRepository.save(Iestade.builder()
                .nosaukums("NOSAUKUMS")
                .adrese("ADRESSE")
                .build());
        Rinda rinda = this.rindaRepository.save(Rinda.builder()
                .iestadeId(iestade)
                .vards("VARDS")
                .uzvards("UZVARDS")
                .personasKods("111111-22222")
                .registracijasDatumsRinda(new Date())
                .prioritate("")
                .build());
        this.mockMvc.perform(
                delete("/rinda/iestade/" + iestade.getIestadeId() + "/persona/" + rinda.getPersonasKods())
        ).andExpect(status().isOk());

        assertNotNull(rindaRepository.findByIestadeIdIestadeIdAndPersonasKods(rinda.getIestadeId().getIestadeId(), rinda.getPersonasKods()));

    }
    @Test
    void sort(){

    }

}