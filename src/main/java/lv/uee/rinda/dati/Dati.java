package lv.uee.rinda.dati;

import lv.uee.rinda.tips.IestadeDto;
import lv.uee.rinda.tips.PersonaDTO;
import lv.uee.rinda.tips.RindaDto;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface Dati {

    public List<IestadeDto> getIestade();

    public IestadeDto getIestadeByIestadeId(Long iestadeId);

    public List<RindaDto> getRindaByIestadeId(Long iestadeId);

    public RindaDto addIestadeRinda(Long iestadeId, PersonaDTO personaDTO);

    public ResponseEntity<?> deleteRinda(Long iestadeId, String personasKods);


}
