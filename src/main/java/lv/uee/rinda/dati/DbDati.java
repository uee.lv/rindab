package lv.uee.rinda.dati;

import lv.uee.rinda.db.model.Iestade;
import lv.uee.rinda.db.model.Rinda;
import lv.uee.rinda.db.repository.IestadeRepository;
import lv.uee.rinda.db.repository.RindaRepository;
import lv.uee.rinda.exception.ResourceBadRequestException;
import lv.uee.rinda.exception.ResourceNotFoundException;
import lv.uee.rinda.tips.IestadeDto;
import lv.uee.rinda.tips.PersonaDTO;
import lv.uee.rinda.tips.RindaDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class DbDati implements Dati {

    Logger logger = LoggerFactory.getLogger(DbDati.class);

    @Autowired
    IestadeRepository iestadeRepository;

    @Autowired
    RindaRepository rindaRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<IestadeDto> getIestade() {
        return iestadeRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(a -> a.getNosaukums()))
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public IestadeDto getIestadeByIestadeId(Long iestadeId) {
        return iestadeRepository.findById(iestadeId)
                .map(iestade -> {
                    return convertToDto(iestade);
                }).orElseThrow(() -> new ResourceNotFoundException("Pēc norādītajiem kritērijiem dati nav atrasti"));
    }

    @Override
    public List<RindaDto> getRindaByIestadeId(Long iestadeId) {

        if (iestadeId == null) {
            throw new ResourceBadRequestException("Norādiet vārdu");
        }

        List<RindaDto> rindas = rindaRepository.findByIestadeIdIestadeId(iestadeId)
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
        if (rindas.isEmpty()) {
            throw new ResourceNotFoundException("Pēc norādītajiem kritērijiem dati nav atrasti");
        }
        return sortRinda(rindas);
    }


    public List<RindaDto> sortRinda(List<RindaDto> rinda) {
        List<RindaDto> rindaDTO = new ArrayList<>();
        rinda
                .stream()
                .filter(f -> !f.getPrioritate().isEmpty() || !f.getPrioritate().isBlank())
                .sorted(Comparator.comparing(s -> s.getRegistracijasDatumsRinda()))
                .forEach(r -> rindaDTO.add(r));
        rinda
                .stream()
                .filter(f -> f.getPrioritate().isEmpty() || f.getPrioritate().isBlank())
                .sorted(Comparator.comparing(s -> s.getRegistracijasDatumsRinda()))
                .forEach(r -> rindaDTO.add(r));
        return rindaDTO;
    }


    @Override
    public RindaDto addIestadeRinda(Long iestadeId, PersonaDTO rindaDTO) {
        return iestadeRepository.findById(iestadeId)
                .map(iestade -> {
                    if (rindaDTO.getVards().isEmpty() || rindaDTO.getVards().isBlank()) {
                        throw new ResourceBadRequestException("Norādiet vārdu");
                    }

                    if (rindaDTO.getUzvards().isEmpty() || rindaDTO.getUzvards().isBlank()) {
                        throw new ResourceBadRequestException("Norādiet uzvārdu");
                    }

                    rindaRepository.findByIestadeIdIestadeIdAndPersonasKods(iestade.getIestadeId(), rindaDTO.getPersonasKods()).map(rinda -> {
                        throw new ResourceNotFoundException("Persona (pk. " + rinda.getPersonasKods() + ") jau reģistrēta: " + iestade.getNosaukums());
                    });

                    Rinda rinda = new Rinda();
                    rinda.setIestadeId(iestade);
                    rinda.setVards(rindaDTO.getVards());
                    rinda.setUzvards(rindaDTO.getUzvards());
                    rinda.setPersonasKods(rindaDTO.getPersonasKods());

                    rinda.setPrioritate(rindaDTO.getPrioritate());
                    rinda.setRegistracijasDatumsRinda(new Date());
                    return convertToDto(rindaRepository.save(rinda));
                }).orElseThrow(() -> new ResourceNotFoundException("Pēc norādītajiem kritērijiem dati nav atrasti"));
    }

    @Override
    public ResponseEntity<?> deleteRinda(Long iestadeId, String personasKods) {
        return rindaRepository.findByIestadeIdIestadeIdAndPersonasKods(iestadeId, personasKods).map(rinda -> {
            rindaRepository.delete(rinda);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Pēc norādītajiem kritērijiem dati nav atrasti"));
    }

    private IestadeDto convertToDto(Iestade iestade) {
        IestadeDto iestadeDto = modelMapper.map(iestade, IestadeDto.class);
        return iestadeDto;
    }

    private RindaDto convertToDto(Rinda rinda) {
        RindaDto rindaDto = modelMapper.map(rinda, RindaDto.class);
        return rindaDto;
    }

}