package lv.uee.rinda.validate;

import java.util.Arrays;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class PrioritateValidator implements ConstraintValidator<Prioritate, String>{

     List<String> prioritate= Arrays.asList("BRĀLIS", "MĀSA", "");

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return prioritate.contains(value);
    }


}
