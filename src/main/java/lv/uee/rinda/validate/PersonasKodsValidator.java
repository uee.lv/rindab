package lv.uee.rinda.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PersonasKodsValidator implements ConstraintValidator<PersonasKods, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        Pattern pattern = Pattern.compile("^([0-9]{1,6})\\-([0-9]{1,5})$");
        Matcher matcher = pattern.matcher(value);
        try {
            if (!matcher.matches()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }

        /*
        String[] array = value.split("-");

        if (array.length == 2) {
            String val1 = array[0];
            String val2 = array[1];
            return true;
        } else {
            return false;
        }

         */


    }


}
