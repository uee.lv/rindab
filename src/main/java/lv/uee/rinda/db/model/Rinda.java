package lv.uee.rinda.db.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lv.uee.rinda.tips.RindaDto;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "rinda")
public class Rinda implements Serializable {

    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "rinda_seq")
    @SequenceGenerator(
            name = "rinda_seq",
            sequenceName = "rinda_seq",
            initialValue = 1,
            allocationSize = 1
    )
    @Column(name = "rinda_id")
    private Long rindaId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date registracijasDatumsRinda;


    @Column(length = 20)
    private String prioritate;

    @Size(max = 100)
    private String vards;

    @Size(max = 100)
    private String uzvards;

    @Size(max = 12)
    private String personasKods;

    @JoinColumn(name = "iestade_id", referencedColumnName = "iestade_id")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Iestade iestadeId;



}
