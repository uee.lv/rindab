package lv.uee.rinda.db.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "iestade")
public class Iestade implements Serializable {

    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "iestade_seq")
    @SequenceGenerator(
            name = "iestade_seq",
            sequenceName = "iestade_seq",
            initialValue = 1,
            allocationSize = 1
    )
    @Column(name = "iestade_id")
    private Long iestadeId;

    @Size(max = 300)
    private String nosaukums;

    @Size(max = 500)
    private String adrese;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "iestadeId")
    @JsonIgnore
    private Collection<Rinda> rindaCollection;


}
