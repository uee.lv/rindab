package lv.uee.rinda.db.repository;

import lv.uee.rinda.db.model.Rinda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RindaRepository extends JpaRepository<Rinda, Long> {
    List<Rinda> findByIestadeIdIestadeId(Long iestadeId);

    Optional<Rinda> findByIestadeIdIestadeIdAndPersonasKods(Long iestadeId, String personasKods);


}
