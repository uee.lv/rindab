package lv.uee.rinda.db.repository;

import lv.uee.rinda.db.model.Iestade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IestadeRepository extends JpaRepository<Iestade, Long> {
}
