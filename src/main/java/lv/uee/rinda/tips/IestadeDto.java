package lv.uee.rinda.tips;

import lombok.Data;

import java.io.Serializable;

@Data
public class IestadeDto implements Serializable {
    private Long iestadeId;
    private String nosaukums;
    private String adrese;

}
