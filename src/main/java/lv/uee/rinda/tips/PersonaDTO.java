package lv.uee.rinda.tips;

import lombok.Data;
import lv.uee.rinda.validate.PersonasKods;
import lv.uee.rinda.validate.Prioritate;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
public class PersonaDTO implements Serializable {

    @Size(max = 100)
    private String vards;
    @Size(max = 100)
    private String uzvards;
    @PersonasKods
    @Size(max = 12)
    private String personasKods;
    @Prioritate
    @Size(max = 20)
    private String prioritate;

}
