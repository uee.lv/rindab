package lv.uee.rinda.tips;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lv.uee.rinda.validate.Prioritate;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Data
public class RindaDto implements Serializable {


    private Long rindaId;
    private Long iestadeId;
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    private Date registracijasDatumsRinda;
    @Prioritate
    private String prioritate;
    private String vards;
    private String uzvards;
    private String personasKods;

}
