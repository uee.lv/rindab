package lv.uee.rinda.rest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @CrossOrigin(origins = "*")
    @GetMapping("/")
    public String index() {
        String html = "        "
                + "<style>\n"
                + "            .w3-theme {\n"
                + "                color:#fff !important; \n"
                + "                background-color:#074b83 !important\n"
                + "            }\n"
                + "            .w3-xxlarge{\n"
                + "                font-size:36px!important\n"
                + "            }\n"
                + "            .w3-center .w3-bar{\n"
                + "                display:inline-block;\n"
                + "                width:auto\n"
                + "            }\n"
                + "            .w3-center .w3-bar-item{\n"
                + "                text-align:center\n"
                + "            }\n"
                + "            .w3-center{\n"
                + "                text-align:center!important\n"
                + "            }\n"
                + "            .w3-padding{\n"
                + "                padding:8px 16px!important\n"
                + "            }\n"
                + "            .w3-container:after,.w3-container:before,.w3-panel:after,.w3-panel:before,.w3-row:after,.w3-row:before,.w3-row-padding:after,.w3-row-padding:before,\n"
                + "            .w3-cell-row:before,.w3-cell-row:after,.w3-clear:after,.w3-clear:before,.w3-bar:before,.w3-bar:after{\n"
                + "                content:\"\";display:table;clear:both\n"
                + "            }\n"
                + "            .w3-container,.w3-panel{\n"
                + "                padding:0.01em 16px\n"
                + "            }\n"
                + "        </style>\n"
                + "        <div class=\"w3-container w3-theme w3-padding\">\n"
                + "            <div class=\"w3-center\">\n"
                + "                <h1 class=\"w3-xxlarge\">API - bērnudārzu rindas</h1>\n"
                + "            </div>\n"
                + "        </div>";

        return html;
    }

}
