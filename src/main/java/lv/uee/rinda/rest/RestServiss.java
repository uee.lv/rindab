package lv.uee.rinda.rest;

import lv.uee.rinda.dati.Dati;
import lv.uee.rinda.dati.DbDati;
import lv.uee.rinda.tips.IestadeDto;
import lv.uee.rinda.tips.PersonaDTO;
import lv.uee.rinda.tips.RindaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
public class RestServiss {

    @Autowired
    Dati dati = new DbDati();

    @CrossOrigin(origins = "*")
    @GetMapping("/iestade")
    public List<IestadeDto> getIestade() {
        return dati.getIestade();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/iestade/{iestadeId}")
    public IestadeDto getIestadeIestadeId(@PathVariable Long iestadeId) {
        return dati.getIestadeByIestadeId(iestadeId);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/rinda/iestade/{iestadeId}")
    public List<RindaDto> getRindaByIestadeId(@PathVariable Long iestadeId) {
        return dati.getRindaByIestadeId(iestadeId);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/rinda/iestade/{iestadeId}")
    public RindaDto addIestadeRinda(@PathVariable Long iestadeId, @Valid @RequestBody PersonaDTO personaDTO) {
            return dati.addIestadeRinda(iestadeId,personaDTO);
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping("/rinda/iestade/{iestadeId}/persona/{personasKods}")
    public ResponseEntity<?> deleteIestadeRinda(@PathVariable Long iestadeId, @PathVariable String personasKods) {
        return dati.deleteRinda(iestadeId,personasKods);
    }

}
