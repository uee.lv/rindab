FROM openjdk:11-jre-slim
EXPOSE 80
ADD /target/rinda-0.0.1.jar app.jar
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar app.jar" ]
